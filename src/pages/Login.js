	import React, { useState, useEffect, useContext } from 'react';
//useContext is used to unpack or deconstruct the value of the UserContext
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import "./Login.css";


export default function Login() {

	const navigate = useNavigate();

	//Allows us to consume the User context object and it's properties to use for user validation
	const { user, setUser } = useContext(UserContext);
	//State hooks to store the values of the input fields
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	//login button
	const [isActive, setIsActive] = useState(true);

	useEffect(() => {
		//Validation to enable submit button 
		if(username !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [username, password])



	function authenticate(e) {
		e.preventDefault();

		fetch('https://backend-capstone3-jfbonganay.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				username: username,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				setUser({ accessToken: data.accessToken });

				Swal.fire({
					title:'Yay!',
					icon: 'success',
					text: 'Successfully Logged in'
				})

				//Getting the user's credentials
				fetch('https://backend-capstone3-jfbonganay.herokuapp.com/users/info', {
					headers: {
						Authorization: `Bearer ${data.accessToken}`
					}
				})
				.then(res => res.json())
				.then(result => {
					console.log(result)
					if(result.isAdmin === true) {
						localStorage.setItem('username', result.username)
						localStorage.setItem('isAdmin', result.isAdmin)
						setUser({
							username: result.username,
							isAdmin: result.isAdmin
						})

						
						navigate('/')
						//dito ohhh

					}else {
						navigate('/')
					}
				})

			}else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}

			setUsername('');
			setPassword('');

		})
	}
	


	return (

		(user.accessToken !== null) ?

		<Navigate to="/" />

		:
		<div className="form-container1">

		<Form className="form1" onSubmit={(e) => authenticate(e)}>
			<h1 className="text-center text-white">Login</h1>
			<Form.Group className="form-group">
				<Form.Label>Username:</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Username"
					required
					value={username}
					onChange={(e) => setUsername(e.target.value)}

				/>
			</Form.Group>

			<Form.Group className="form-group1">
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter your password"
					required
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					
				/>
			</Form.Group>
			
			{isActive ?
				<Button variant="primary" type="submit" className="mt-3 button"> Login </Button>

				:

				<Button variant="danger" type="submit" disabled className="mt-3 button2"> Login </Button>
			}

			
		</Form>
		</div>

		)
}
