import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import './Register.css';
export default function Register() {

		const navigate = useNavigate();

	//State hooks to store the values of the input fields
		
	const [fullname, setFullname] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [username, setUsername] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	//State to determine whether submit button is enabled or not for conditional rendering
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		//Validation to enable submit button when all fields are populated and both passwords match
		if((fullname !== '' && mobileNo !=='' && username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [fullname,mobileNo,username,email, password1, password2])


	function userReg(e) {
		//Prevents page redirection via form submission
		e.preventDefault();
		fetch('https://backend-capstone3-jfbonganay.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				fullname:fullname,
				mobileNo:mobileNo,
				email:email,
				username: username,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
				title: "Yaaaaaaaaay!",
				icon: "success",
				text: "You have successfully registered"
				})


				//Getting the user's credentials
				//redirect the admin to /courses
				navigate('/login')

					
				

			}else {
				Swal.fire({
					title: 'Ooops!',
					icon: 'error',
					text: 'Something Went Wrong. Check your Credentials.'
				})
			}

			setFullname('');
			setMobileNo('');
			setUsername('');
			setEmail('');
			setPassword1('');
			setPassword2('');

		})
	}
		

		
	

	return (

		//Conditional rendering
		
		<div className="form-container">

		<Form className="form" onSubmit={(e) => userReg(e)}>
			<h1 className="text-white text-center">Register</h1>
			<Form.Group className="form-group"> 
				<Form.Label>Full Name</Form.Label>
				<Form.Control 

					type="text"
					placeholder="First Last"
					required
					value={fullname}
					onChange={e => setFullname(e.target.value)}
					/>
					
			</Form.Group>
			<Form.Group className="form-group"> 
				<Form.Label>Phone Number</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Please enter your phone number"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					/>
					
			</Form.Group>
			<Form.Group className="form-group"> 
				<Form.Label>UserName</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Username"
					required
					value={username}
					onChange={e => setUsername(e.target.value)}
					/>
					
			</Form.Group>
			<Form.Group className="form-group"> 
				<Form.Label>Email Address</Form.Label>
				<Form.Control 
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					
			</Form.Group>

			<Form.Group className="form-group"> 
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					/>
			</Form.Group>

			<Form.Group className="form-group"> 
				<Form.Label>Verify Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					/>
			</Form.Group>
			<Form.Group className=" text-light">
			  <Form.Check 
			    type="switch"
			    id="custom-switch"
			    label="I'd like to receive exclusive offers and promotions via SMS"
			  />
			  </Form.Group>


			{isActive ? 
				<Button variant="primary" type="submit" className="mt-3">SIGN UP</Button>

				:

				<Button variant="primary" type="submit" className="mt-3" disabled>SIGN UP</Button>

			}
			<Form.Text className="text-muted">
			By clicking "SIGN UP"; I agree to アニメの置物's Terms of Use and Privacy Policy  
			</Form.Text>

			
		</Form>
		</div>












		)
}