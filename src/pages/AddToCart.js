import React, { useContext, useState, useEffect} from 'react';
import UserView from '../components/UserView';
import UserContext from '../UserContext';
import {useParams,useNavigate, Link} from 'react-router-dom';

export default function ProductList(){
	const {user} = useContext(UserContext);
	
    const navigate = useNavigate();
	const { userId } = useParams();
	const [allProducts, setAllProducts] = useState([]);

    const fetchData = () => {
		fetch(`https://backend-capstone3-jfbonganay.herokuapp.com/orders/${userId}`,{
		method:'GET',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllProducts(data)
		})
	}

   useEffect(() => {
		fetchData();
	}, [])

return(

		 <>

            
                <UserView productsData={allProducts}/>
                
                
            
        </>
        	)
}

