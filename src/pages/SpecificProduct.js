import React, { useState, useContext, useEffect } from 'react';
import {Container,Card,Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import {useParams,useNavigate, Link} from 'react-router-dom';
import pic from '../components/images/Figures_Banner.jpg';
import Swal from 'sweetalert2';
import './Login.css';
export default function SpecificProduct(){
    
    const {user} = useContext(UserContext);
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the courseId passed via the URL
	const { productId } = useParams();
    useEffect(() => {
		fetch(`https://backend-capstone3-jfbonganay.herokuapp.com/products/${productId}`, {
			method:'GET',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
            setQuantity(data.quantity)
		})
	}, [])
	const order = (productId) => {
		fetch('https://backend-capstone3-jfbonganay.herokuapp.com/orders/order', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Successfully added to cart',
					icon: 'success'
				})

				navigate('/cart')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


    
    return(
        <Container>
			<Card className='shadow-lg m-2 p-3 rounded align text-center card' border="primary" >
                    <Card.Body>
                        <Card.Img variant="top" src={pic} alt="" />
                        <Card.Text><h5>{name}</h5></Card.Text>
                        <Card.Text><p>Description: {description}</p></Card.Text>
                        <Card.Text><p>Price:¥{price}</p></Card.Text>
                        <Card.Text><p>Available: {quantity}</p></Card.Text>
                    </Card.Body>
				<Card.Footer>
				{user.accessToken !== null ?
					<div className="d-grip gap-2">
						<Button   className ="button mt-3" onClick={() => order(productId)}>Add to Cart</Button><br></br>
						<Button variant="info" className ="button2 mt-3">Add to WishList</Button>

					</div>		
					:
					<Link className="button d-grip gap-2" to="/login">Login to Purchase</Link>
				}
				</Card.Footer>
			</Card>
		</Container>
    )
}

 