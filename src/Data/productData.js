const productsData = [
	{
		id: "wdc001",
		item: "PHP - Laravel",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		item: "Python - Django",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		item: "Java - Springboot",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Similique aperiam nihil labore incidunt nesciunt saepe exercitationem eveniet voluptatum, soluta eos facilis excepturi ipsa consectetur impedit cum fugiat laborum deleniti sit!",
		price: 55000,
		onOffer: true
	}
]
export default productsData;