import React, {  useContext } from 'react';
import { Navbar, NavDropdown, Form, FormControl, Button, Nav } from 'react-bootstrap';
import { Link} from "react-router-dom";
import Container from 'react-bootstrap/Container';
import UserContext from '../UserContext';
import { BiUserCircle,BiCartAlt, BiHomeSmile } from "react-icons/bi";
import pic from "./images/anime_figurine.png"
import '../App.css';
import {FaBoxOpen} from "react-icons/fa";
export default function AppNavbar() {
    const { user } = useContext(UserContext);

        return (
                <Navbar collapseOnSelect className="simple-linear" variant="light" expand="lg" color="=black" sticky="top">
                      <Container fluid>
                      <img src={pic} alt = '' height={50} width={50} />
                        <Navbar.Brand href="/">アニメの置物</Navbar.Brand>
                      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                      <Navbar.Collapse id="responsive-navbar-nav">
                        
                        
                      <Nav className="d-flex ms-auto">
                          <Nav.Link as={Link} to="/"><BiHomeSmile size={25} /></Nav.Link>
                          <Nav.Link as={Link} to="/products"><FaBoxOpen size={25} /></Nav.Link>
                          <Nav.Link as={Link} to="/cart"><BiCartAlt size={25} /></Nav.Link>
                          <NavDropdown title=< BiUserCircle size={25} />id="basic-nav-dropdown">
                            { (user.accessToken !== null) ?
                            <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
                               :
                               <>
                            <NavDropdown.Item href="/login">Login</NavDropdown.Item>
                            <NavDropdown.Item href="/register">Register</NavDropdown.Item>
                            </>
                          }
                            </NavDropdown>
                            

                        </Nav>
                        <Nav >
                          <Form className ="d-flex" >
                            <FormControl
                              type="search"
                              placeholder="Search"
                              className="me-2"
                              aria-label="Search"
                            />
                            <Button variant="outline-info">Search</Button>
                          </Form>
                          </Nav>
                      </Navbar.Collapse>
                      </Container>
                    </Navbar>

                  
                
        )
    }
