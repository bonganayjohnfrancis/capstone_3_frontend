import React, {useState, useEffect} from 'react';
import ProductCard from './ProductCard';
import {Container} from 'react-bootstrap'

export default function UserView({productsData}){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArray = productsData.map(product => {
			//only render the active courses
			if(product.isActive === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>

				)
			} else {
				return null;
			}
		})
		//set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
		setProducts(productsArray)
	},[productsData])


	return(
		<>
		 <div  className="justify-content-center p-2">
               <h1 className='text-center'>Show All Products</h1>
               <div className="row">
			{products}
			</div>
			</div>
		</>
	)
}
