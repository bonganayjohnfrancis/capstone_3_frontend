import React from 'react';
import {Card,Col,Image} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './ProductCard.css';
import pic1 from './images/Figures_Banner.jpg'
export default function ProductCard({productProp}){
    const { _id, name, description, price, quantity, picture } = productProp

    return( 
                <Col xs={12} md={6} lg={3}>
                <Card className='shadow-lg m-2 p-3 rounded align text-center card'  >
                    <Card.Body>
                        <Card.Img variant="top" src={pic1} alt="" />
                        <Card.Text><h5>{name.slice(0,20)}...</h5></Card.Text>
                        <Card.Text><p>Description: {description.slice(0,10)}...</p></Card.Text>
                        <Card.Text><p>Price:¥{price}</p></Card.Text>
                        <Card.Text><p>Available: {quantity}</p></Card.Text>
                        <Link className="btn btn-primary button " to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card> 
                </Col>

    ) 
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired
    })
}
